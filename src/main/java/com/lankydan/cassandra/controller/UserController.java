package com.lankydan.cassandra.controller;

import com.lankydan.cassandra.actor.ActorRepository;
import com.lankydan.cassandra.actor.entity.Actor;
import com.lankydan.cassandra.actor.entity.ActorByMovieKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private ActorRepository actorRepository;


    @GetMapping("/actors/{id}")
    public ResponseEntity<Actor> getActorById(@PathVariable(required = false) UUID id){
        try{
            Optional<Actor> actor = actorRepository.findByActorId(id);
            if(!actor .isPresent())
                return new ResponseEntity<Actor>(HttpStatus.NO_CONTENT);

            return new ResponseEntity<>(actor.get(), HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/actors")
    public ResponseEntity<List<Actor>> getAllTutorials() {
        try {
            List<Actor> actors = actorRepository.findAll();

            if (actors.isEmpty()) {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            return new ResponseEntity<>(actors, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}